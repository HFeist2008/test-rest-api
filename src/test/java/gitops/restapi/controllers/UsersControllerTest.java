package gitops.restapi.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import gitops.restapi.models.db.User;
import gitops.restapi.models.repositories.UserRepository;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsersControllerTest {

    private final Api api;
    private final UserRepository repo;
    private final DataSource dataSource;

    @Autowired
    public UsersControllerTest(
        @LocalServerPort int port,
        @Autowired ObjectMapper objectMapper,
        @Autowired UserRepository repo,
        @Autowired DataSource dataSource
    ) {
        this.api = Api.create("http://localhost:" + port, objectMapper);
        this.repo = repo;
        this.dataSource = dataSource;
    }

    private User buildUser(String firstName, String lastName, int age, String email) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAge(age);
        user.setEmail(email);
        return user;
    }

    @BeforeEach
    public void setUp() throws Exception {
        repo.deleteAll();
        dataSource.getConnection().prepareStatement("alter sequence \"seq\" restart with 1").execute();
        repo.saveAllAndFlush(Arrays.asList(
          buildUser("Martin", "Boßlet", 42, "martin.bosslet@gmail.com"),
          buildUser("Max", "Mustermann", 21, "max@example.org")  
        ));
    }

    @AfterEach
    public void cleanUp() {
        repo.deleteAll();
    }

    private interface Api {
        @GET("/api/users")
        Call<List<User>> getAllUsers();

        @GET("/api/users/{id}")
        Call<User> getUser(@Path("id") int id);

        @POST("/api/users")
        Call<User> createUser(@Body User toCreate);

        @PUT("/api/users/{id}")
        Call<User> updateUser(@Path("id") int id, @Body User update);

        @DELETE("/api/users/{id}")
        Call<Void> deleteUser(@Path("id") int id);

        static Api create(String baseUrl, ObjectMapper objectMapper) {
            return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .build()
                .create(Api.class);
        }
    }

    @Test
    public void getAllUsers() throws IOException {
        Response<List<User>> response = api.getAllUsers().execute();
        assertThat(response.code()).isEqualTo(200);
        List<User> users = response.body();
        assertThat(users).isNotNull();
        assertThat(users).hasSize(2);
        List<String> lastNames = users.stream().map(User::getLastName).collect(Collectors.toList());
        assertThat(lastNames).containsExactlyInAnyOrder("Boßlet", "Mustermann");
    }

    @Test
    public void getUser() throws IOException {
        Response<User> response = api.getUser(1).execute();
        assertThat(response.code()).isEqualTo(200);
        User user = response.body();
        assertThat(user).isNotNull();
        assertThat(user.getFirstName()).isEqualTo("Martin");
        assertThat(user.getLastName()).isEqualTo("Boßlet");
    }

    @Test
    public void createUser() throws IOException {
        String firstName = "Heidi";
        String lastName = "Müller";
        int age = 32;
        String email = "heidi.müller@gmx.de";
        User toCreate = new User();
        toCreate.setFirstName(firstName);
        toCreate.setLastName(lastName);
        toCreate.setAge(age);
        toCreate.setEmail(email);
        Response<User> response = api.createUser(toCreate).execute();
        assertThat(response.code()).isEqualTo(201);
        String location = response.headers().get("Location");
        assertThat(location).isEqualTo("/api/users/3");
        User user = response.body();
        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo(3L);
        assertThat(user.getFirstName()).isEqualTo(firstName);
        assertThat(user.getLastName()).isEqualTo(lastName);
        assertThat(user.getAge()).isEqualTo(age);
        assertThat(user.getEmail()).isEqualTo(email);
    }

    @Test
    public void updateUser() throws IOException {
        String firstName = "Anna";
        String lastName = "Boßlet";
        int age = 4;
        String email = "anna.bosslet@example.org";
        User update = new User();
        update.setFirstName(firstName);
        update.setLastName(lastName);
        update.setAge(age);
        update.setEmail(email);
        Response<User> response = api.updateUser(1, update).execute();
        assertThat(response.code()).isEqualTo(200);
        User user = response.body();
        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo(1L);
        assertThat(user.getFirstName()).isEqualTo(firstName);
        assertThat(user.getLastName()).isEqualTo(lastName);
        assertThat(user.getAge()).isEqualTo(age);
        assertThat(user.getEmail()).isEqualTo(email);
    }

    @Test
    public void deleteUser() throws IOException {
        Response<Void> response = api.deleteUser(2).execute();
        assertThat(response.code()).isEqualTo(200);
        Response<User> notFound = api.getUser(2).execute();
        assertThat(notFound.code()).isEqualTo(404);
    }
}
