FROM eclipse-temurin:17-jdk

ENV BASE_DIR /app
RUN mkdir -p $BASE_DIR
ENV GRADLE_VERSION=gradle-7.5.1

WORKDIR $BASE_DIR

RUN addgroup --system java && adduser --system --group java &&\
    apt update -y &&\
    apt install -y unzip &&\
    curl -L https://services.gradle.org/distributions/$GRADLE_VERSION-bin.zip -o /app/$GRADLE_VERSION-bin.zip &&\
    unzip /app/$GRADLE_VERSION-bin.zip -d /app


COPY build.gradle build.gradle

# To use gradle wrapper instead of download:
# COPY gradlew gradlew
# COPY .gradle .gradle
# COPY gradle gradle

COPY docker docker
COPY src src

RUN chown -R java:java $BASE_DIR 
USER java:java

RUN /app/$GRADLE_VERSION/bin/gradle build -x test &&\
    /app/$GRADLE_VERSION/bin/gradle testClasses &&\
    cp /app/build/libs/app-1.0.0.jar app.jar

EXPOSE 8080

CMD ["/app/docker/run.sh"]