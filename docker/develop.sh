#!/bin/bash

cd /app

export GRADLE_HOME=/app/gradle-7.5.1
# Connect with debugger on port 5005
SPRING_PROFILES_ACTIVE=development $GRADLE_HOME/bin/gradle bootRun
